import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { TaskService } from './services/task.service';
import { EffortService } from './services/effort.service';


const appRoutes: Routes = [
  { path: '', component: EmployeeListComponent },
  { path: 'timesheet', component: TimesheetComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    EmployeeService,
    TaskService,
    EffortService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
