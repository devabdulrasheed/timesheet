import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'employee-list',
  templateUrl: 'employee.component.html',
  styleUrls: ['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
  employees: any;
  constructor(
    private employeeService: EmployeeService,
    private router: Router
  ) { }

  ngOnInit() {
    this.employeeService.GetEmployeesWorkEffort().subscribe(data => {
      this.employees = data;
    });
  }
  setClickedRow(empcode) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        'empcode': empcode
      }
    };

    this.router.navigate(['/timesheet'], navigationExtras);
  }
}
