import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EffortService {
  private baseapi = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getweeklylog(empcode) {
    return this.http.get(this.baseapi + '/effort/get/' + empcode);
  }

  neweffort(bodyData) {
    return this.http.post(this.baseapi + '/effort/addnew', bodyData);
  }
}
