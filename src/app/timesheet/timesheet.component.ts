import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { EmployeeService } from '../services/employee.service';
import { TaskService } from '../services/task.service';
import { EffortService } from '../services/effort.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';


@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {

  employees: any;
  tasks: any;
  effort_log: any;

  public rows: Array<{ task: string, sun: 0, mon: 0, tue: 0, wed: 0, thu: 0, fri: 0, sat: 0 }> = [];

  private sub: any;

  public st: number;
  public mt: number;
  public tut: number;
  public wt: number;
  public tht: number;
  public ft: number;
  public sat: number;

  private newValue: any = {};
  private isAddNew: boolean;
  private employeeCode: string;
  private nrSelect: string;
  constructor(
    private employeeService: EmployeeService,
    private taskService: TaskService,
    private EffortService: EffortService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {




    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;
    });

    this.taskService.getalltasklists().subscribe(data => {
      this.tasks = data;
    });

    this.sub = this.route.queryParams.subscribe(params => {
      this.employeeCode = params["empcode"];

      if (this.employeeCode) {
        this.geteffortData();
      }
    });
  }

  geteffortData() {



    this.EffortService.getweeklylog(this.employeeCode).subscribe(data => {
      this.tbassing(data);
    });


    this.nrSelect = this.employeeCode;
  }
  tbassing(data) {
    this.st = 0;
    this.mt = 0;
    this.tut = 0;
    this.wt = 0;
    this.tht = 0;
    this.ft = 0;
    this.sat = 0;
    if (data) {
      for (const row of data) {
        this.st += row.sunday;
        this.mt += row.monday;
        this.tut += row.tuesday;
        this.wt += row.wednesday;
        this.tht += row.thursday;
        this.ft += row.friday;
        this.sat += row.saturday;
        console.log(row.sunday);
      }
      this.effort_log = data;
    }
  }
  onChange(employeeCode) {


    const navigationExtras: NavigationExtras = {
      queryParams: {
        'empcode': employeeCode
      }
    };

    this.router.navigate(['/timesheet'], navigationExtras);

  }



  add() {
    if (this.isAddNew) {
      this.isAddNew = false;
      this.rows = [];
    } else {
      this.isAddNew = true;
      this.rows.push(this.newValue);
      this.newValue = {};
      this.newValue.EmployeeCode = this.employeeCode;
    }
  }

  save() {
    this.isAddNew = false;
    this.EffortService.neweffort(this.newValue).subscribe(data => {
      this.tbassing(data);
    });
    this.rows = [];
  }
}
